#include "BulletObject.h"
#include "MyProjectMain.h"
#include "SoldierObject.h"
#include "EnemyObject.h"

#define Right 0
#define Left 1
#define Up 2
#define Down 3


BulletObject::BulletObject(MyProjectMain* pEngine)
	: DisplayableObject(pEngine)
	, m_pMainEngine(pEngine)
	, direction(3)
	, iMapX(1)
	, iMapY(1)
	, fire(0)
	, pDirection(3)
	, bomb(0)
	, bombStart(0)
{
	
	m_iDrawWidth = 20;
	m_iDrawHeight = 20;
	m_iStartDrawPosX = 0;
	m_iStartDrawPosY = 0;

	DisplayableObject* pObject;
	pObject = m_pMainEngine->GetDisplayableObject(0);
	m_iPreviousScreenX = m_iCurrentScreenX = pObject->GetXCentre() - 10;
	m_iPreviousScreenY = m_iCurrentScreenY = pObject->GetYCentre() - 10;

	im.LoadImage( "bullet1right.png" );
	im1.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bullet1left.png" );
	im2.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bullet1up.png" );
	im3.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bullet1down.png" );
	im4.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);

	SetVisible(true);
}


BulletObject::~BulletObject(void)
{
}


void BulletObject::Draw(void)
{
	if(bomb){
	im.LoadImage( "bomb1.png" );
	im5.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bomb2.png" );
	im6.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bomb3.png" );
	im7.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bomb4.png" );
	im8.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);

	// draw the image if the bullet bombed
		switch(bomb)
		{
		case 1:

			im5.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY, 
					im5.GetWidth(), im5.GetHeight());
			break;
		case 2:

			im6.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY, 
					im6.GetWidth(), im6.GetHeight());
			break;
		case 3:

			im7.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY, 
					im7.GetWidth(), im.GetHeight());
			break;
		case 4:

			im8.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY, 
					im8.GetWidth(), im8.GetHeight());
		}
		
	}
	else
	{
	
	// draw the picture when the bullet is fired
	if(fire)
	{
		// draw the fire picture based on the direction the bullet aimed to
		switch(direction)
		{
		case 0: //right
			im1.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY, 
					im1.GetWidth(), im1.GetHeight());
			break;
		case 1: //left
			im2.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX, m_iCurrentScreenY,  
					im2.GetWidth(), im2.GetHeight());
			break;
		case 2: //up
			im3.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im3.GetWidth(), im3.GetHeight());
			break;
		case 3: //down
			im4.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX ,m_iCurrentScreenY,  
					im4.GetWidth(), im4.GetHeight());
			break;
		}
	}
	}
	StoreLastScreenPositionAndUpdateRect();
}


void BulletObject::DoUpdate(int iCurrentTime)
{

	m_iPreviousScreenX = m_iCurrentScreenX;
	m_iPreviousScreenY = m_iCurrentScreenY;
	// get the direction of the soldier
	DisplayableObject* pObject = m_pMainEngine -> GetDisplayableObject(0);
	int directionofSoldier = ((SoldierObject*) pObject)->getDirection();
	switch(directionofSoldier)
	{
	case 1:
		pDirection = Up;
		break;
	case 2:
		pDirection = Down;
		break;
	case 3:
		pDirection = Left;
		break;
	case 4:
		pDirection = Right;
		break;
	}
	// decide the picture to draw during the bomb, each picture will remain 100 milisecond on the screen
	if(bomb)
	{
		if(iCurrentTime < bombStart + 100)
		{
			bomb = 1;
		}
		if(iCurrentTime < bombStart + 200 && iCurrentTime >= bombStart + 100)
		{
			bomb = 2;
		}
		if(iCurrentTime < bombStart + 300 && iCurrentTime >= bombStart + 200)
		{
			bomb = 3;
		}
		if(iCurrentTime < bombStart + 400 && iCurrentTime >= bombStart + 300)
		{
			bomb = 4;
		}
		
		// stop bomb and change to not fire and ready to fire
		if(iCurrentTime >= bombStart + 400)
		{
			m_iCurrentScreenX = pObject-> GetXCentre() - 10;
			m_iCurrentScreenY = pObject-> GetYCentre() - 10;
			fire = 0;
			bomb = 0;
		}
	}

	else{
	int Xdirection = 0;
	int Ydirection = 0;
	iMapX = (m_iCurrentScreenX + 10 - 25) / 20;
	iMapY = (m_iCurrentScreenY + 10 - 60) / 20;
	MyProjectTileManager& tm = m_pMainEngine -> GetTileManager();
	// the bullet would destroy the fort and the tree
	switch ( tm.GetValue( iMapX, iMapY ) )
	{
	case 2:	
		// update the tile since the tree has been destroyed
		tm.UpdateTile( m_pMainEngine, iMapX, iMapY, 7 );
		bomb = 1;
		bombStart = iCurrentTime;

		break;
	case 4:
		// update the tile since the fort has been destroyed
		tm.UpdateTile( m_pMainEngine, iMapX, iMapY, 8 );
		bomb = 1;
		bombStart = iCurrentTime;

		break;
	}

	

	int detaX = abs(m_iCurrentScreenX - (pObject->GetXCentre() - 10));
	int detaY = abs(m_iCurrentScreenY - (pObject-> GetYCentre() - 10));

	// a criteria to measure whether the bullet is fire or not
	// if detaX + detaY > 5, the bullet is fire
	if(detaX + detaY > 5)
	{
		// get the direction of the bullet of current fire
		switch(direction)
		{
		case Up:
			Ydirection = -1;
			break;
		case Down: 
			Ydirection = 1;
			break;
		case Right: 
			Xdirection = 1;
			break;
		case Left: 
			Xdirection = -1;
			break;
		}

		// get the value of the tile check whether it is able to move across
		switch ( tm.GetValue( 
				iMapX + Xdirection,
				iMapY + Ydirection ) )
		{
		case 0: // road
		case 2: // tree
		case 3: // river
		case 4: // fort
		case 5: // buff
		case 6: // destination
		case 7: // passageway
		case 8: // passageway
		case 9: // passageway
			// Allow move - set up new movement now
			m_iCurrentScreenX += Xdirection * 10;
			m_iCurrentScreenY += Ydirection * 10;
			break;	
		case 1: // building
			// not allow to move and bomb
			bomb = 1;
			bombStart = iCurrentTime;
			break;
		}

		// check whether the bullet shot at the enemy
		int level = m_pMainEngine->getLevel();
		for(int i = 2; i < 2*level + 3; i++)
		{
			EnemyObject* pObject = dynamic_cast <EnemyObject*> (m_pMainEngine->GetDisplayableObject(i));
			if(pObject == NULL)
			{
				return;
			}
			else{
			int DitaX1 = abs (this->GetXCentre() - pObject->GetXCentre());
			int DitaY1 = abs (this->GetYCentre() - pObject->GetYCentre());
			int validEnemy  = pObject -> Islive();
			if ((DitaX1 * DitaX1 + DitaY1 * DitaY1 < 15 * 15) && validEnemy)
			{
				bomb = 1;
				bombStart = iCurrentTime;
				break;
			}
			}
		}

	}
	// not fire
	else
	{
		// when the key 'Z' is pressed
		if(GetEngine() ->IsKeyPressed( SDLK_z))
		{
			fire = 1;
			direction = pDirection;

			// set the initial movement
			switch(direction)
			{
			case Up: m_iCurrentScreenY -= 5; break;
			case Down: m_iCurrentScreenY += 5; break;
			case Left: m_iCurrentScreenX -= 5; break;
			case Right: m_iCurrentScreenX += 5; break;
			}
		}

		// not fire
		else
		{
			m_iCurrentScreenX = pObject->GetXCentre() - 10;
			m_iCurrentScreenY = pObject->GetYCentre() - 10;
		}
	}
	}
	RedrawObjects();
}


int BulletObject::IsFire(void)
{
	return fire;
}
