#pragma once
#include "MovementPosition.h"
#include "displayableobject.h"
#include "JPGImage.h"

class MyProjectMain;

class BulletObject :
	public DisplayableObject
{

private:
	MyProjectMain* m_pMainEngine;
	int direction;
	MovementPosition m_oMover;
	int iMapX;
	int iMapY;
	int fire;
	int pDirection;
	int bomb;
	ImageData im1, im2, im3, im4, im5, im6, im7, im8, im;
	int bombStart;


public:
	BulletObject(MyProjectMain* pEgine);
	~BulletObject(void);
	void Draw(void);
	void DoUpdate(int iCurrentTime);
	int IsFire(void);
};

