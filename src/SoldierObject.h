#pragma once
#include "displayableobject.h"
#include "MovementPosition.h"
#include "JPGImage.h"


class MyProjectMain;

class SoldierObject :
	public DisplayableObject
{

private:
	MyProjectMain* m_pMainEngine;
	MovementPosition m_oMover;
	int m_iMapX;
	int m_iMapY;
	int direction;
	ImageData im1, im2, im3, im4, im5, im6, im7, im8, im, im0;
	int imgIndex;
	int speed;
	int buffTime;
	int hasBuff;
	int IsDied(void);

public:
	
	SoldierObject(MyProjectMain* pEngine);
	~SoldierObject(void);
	void Draw(void);
	void DoUpdate(int iCurrentTime);
	//int getPreviousX();
	//int getPreviousY();
	int getMapX(){return m_iMapX;}
	int getMapY(){return m_iMapY;}
	int getDirection(){return direction;}


};
