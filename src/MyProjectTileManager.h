#pragma once
#include "tilemanager.h"
class MyProjectTileManager :
	public TileManager
{
public:
	MyProjectTileManager(void);
	~MyProjectTileManager(void);
	int GetTileWidth(void);
	int GetTileHeight(void);
	void DrawTileAt(BaseEngine* pEngine, SDL_Surface* pSurface, int iMapX, int iMapY, int iStartPositionScreenX, int iStartPositionY);

	// Get a value for a specific tile
	int GetValue(int iMapX, int iMapY)
	{
		if ( m_pData == NULL )
			return -1;
		if ( iMapX >= m_iMapWidth )
			return -2;
		if ( iMapY >= m_iMapHeight )
			return -3;

		return m_pData[ iMapX + iMapY * m_iMapWidth ];
	}

	// Set the value of a specific tile
	void SetValue(int iMapX, int iMapY, int iValue)
	{
		if ( m_pData == NULL )
			return;
		if ( iMapX >= m_iMapWidth )
			return;
		if ( iMapY >= m_iMapHeight )
			return;

		m_pData[ iMapX + iMapY * m_iMapWidth ] = iValue;
	}

};

