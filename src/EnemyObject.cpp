#include "EnemyObject.h"
#include "DisplayableObject.h"
#include "MyProjectMain.h"
#include "SoldierObject.h"
#include "BulletObject.h"

/*
#define up 0;
#define down 1;
#define left 2;
#define right 3;
*/

EnemyObject::EnemyObject(MyProjectMain* eEngine)
	:DisplayableObject(eEngine)
	, m_pMainEngine(eEngine)
	, iMapX(0)
	, iMapY(0)
	,live(1)
	, action(0)
	, direction(1)
	, imgIndex(0)
	, actionTime(0)
	, MoveChoice(0)
{
	// initialise the start position for the enemy object
	initialPosition();

	m_iStartDrawPosX = 0;
	m_iStartDrawPosY = 0;
	m_iDrawWidth = 20;
	m_iDrawHeight = 20;

	im.LoadImage( "mup1.png" );
	im1.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mup2.png" );
	im2.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mdown1.png" );
	im3.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mdown2.png" );
	im4.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mleft1.png" );
	im5.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mleft2.png" );
	im6.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mright1.png" );
	im7.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "mright2.png" );
	im8.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "bomb4.png" );
	im0.ResizeTo(&im, 100, 100, false);

	SetVisible(true);
}


void EnemyObject::initialPosition(void)
{
	// randomly generate the initial position for the enemy object
	iMapX = rand()% 29 + 8;
	iMapY = rand()% 20 + 8;
	int invalid = 1;
	MyProjectTileManager& tm = m_pMainEngine -> GetTileManager();
	// asume it is invalid first and to check whether it is valid
	while(invalid){
		switch ( tm.GetValue( 
				iMapX ,
				iMapY) )
		{
		case 1: // invalid position, the enemy can not stand on the tiles with value 1
		case 2: // invalid position, the enemy can not stand on the tiles with value 2
		case 3: // invalid position, the enemy can not stand on the tiles with value 3
		case 4: // invalid position, the enemy can not stand on the tiles with value 4
			// random generate a new postion
			iMapX = rand()% 29 + 8;
			iMapY = rand()% 20 + 8;
			break;
		case 0: // valid position
		case 5: // valid position
		case 6: // valid position
		case 7: // valid position
		case 8: // valid position
			// change the invalid value and jump out of the loop
			invalid = 0;
			break;
		default:
			invalid = 0;
			break;
		}
	}
	// set the position according to tile
	m_iPreviousScreenX = m_iCurrentScreenX = iMapX * 20 + 25;
	m_iPreviousScreenY = m_iCurrentScreenY = iMapY * 20 + 60;
}


EnemyObject::~EnemyObject(void)
{
}


void EnemyObject::Draw(void)
{
	// draw the image only when the enemy object is still alive
	if(live)
	{
		
		if(action == 2)
		{
			m_iDrawWidth = 100;
			m_iDrawHeight = 100;
			m_iCurrentScreenX = m_iCurrentScreenX - 40;
			m_iCurrentScreenY = m_iCurrentScreenY - 40;
			if(m_iCurrentScreenX < 45){m_iCurrentScreenX = 45;}
			if(m_iCurrentScreenY < 80){m_iCurrentScreenY = 80;}
			im0.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im0.GetWidth(), im0.GetHeight());
		}
		else{
		
			switch(direction)
			{
				m_iDrawWidth = 20;
				m_iDrawHeight = 20;
			// when the direction of enemy object is up
			case 0:
				// imgIndex = 0 if (iCurrentTime % 400 <200), imgIndex = 1 otherwise
				// hence, for one direction, there are two pictures to switch to make the objects seem running
				if(imgIndex)
					im1.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im1.GetWidth(), im1.GetHeight());
				else
					im2.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im2.GetWidth(), im2.GetHeight());
				break;
				
			// when the direction of enemy object is down
			case 1:
				if(imgIndex)
					im3.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im3.GetWidth(), im3.GetHeight());
				else
					im4.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im4.GetWidth(), im4.GetHeight());
				break;

			// when the direction of enemy object is left
			case 2:
				if(imgIndex)
					im5.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im5.GetWidth(), im5.GetHeight());
				else
					im6.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im6.GetWidth(), im6.GetHeight());
		
				break;
	
			// when the direction of enemy object is right
			case 3:
				if(imgIndex)
					im7.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im7.GetWidth(), im7.GetHeight());
				else
					im8.RenderImageWithMask( m_pEngine->GetForeground(), 
						0, 0, 
						m_iCurrentScreenX,m_iCurrentScreenY, 
						im8.GetWidth(), im8.GetHeight());
				break;
	
			}
		}
		StoreLastScreenPositionAndUpdateRect();	
	}
	
}





void EnemyObject::DoUpdate(int iCurrentTime)
{
	m_iPreviousScreenX = m_iCurrentScreenX;
	m_iPreviousScreenY = m_iCurrentScreenY;
	



	// change the value of imgIndex referring to iCurrentTime to switch the pictures
	if(iCurrentTime % 400 < 200)
	{
		imgIndex = 1;
	}
	else{
		imgIndex = 0;
	}
	
	// check whether the enemy object is shooted by the player or not
	CheckLive();
	
	
	
	if(action >= 2 )
	{
		if(action > 3)
		{
			live = 0;
			action = 0;
		}
		action++;
	} 
	else
	{
		
		// check whether the enemy object observe the player or not
	// if does, the enemy will do the ActionMove, else do the NonActionMove
	
		IsAction(iCurrentTime);
	
		if(action == 1)
		{
			if(iCurrentTime > actionTime + 20000)
			{
				action = 2;
				
			}
		}
	
		SoldierObject*  pObject = dynamic_cast <SoldierObject*>  (m_pMainEngine->GetDisplayableObject(0));
		if(pObject == NULL)
		{
			return;
		}

		if(live)
		{
			if(m_oMover.HasMovementFinished(iCurrentTime))
			{
				if(!action)
				{
					// move to the postion near-by randomly with low speed
					NonActionMove();
					m_oMover.Setup(
						m_iCurrentScreenX,
						m_iCurrentScreenY,
						iMapX * 20 + 25,
						iMapY * 20 + 60,
						iCurrentTime,
						iCurrentTime + 700);
				}
				if(action == 1)
				{
					// move towards to the soldier
					int move = ActionMove(iCurrentTime);
					if(iMapX < 1 || iMapX > 36 || iMapY < 1 || iMapY > 26)
					{
						live = 0;
					}
					else
					{
						m_oMover.Setup(
						m_iCurrentScreenX,
						m_iCurrentScreenY,
						iMapX * 20 + 25,
						iMapY * 20 + 60,
						iCurrentTime,
						iCurrentTime + 300 * move);
					}
					
				}
			}
			if ( !m_oMover.HasMovementFinished(iCurrentTime) )
			{
				// Ask the mover where the object should be
				m_oMover.Calculate( iCurrentTime );
				m_iCurrentScreenX = m_oMover.GetX();
				m_iCurrentScreenY = m_oMover.GetY();
			}
		}
	}
	RedrawObjects();
}




void EnemyObject::CheckLive(void)
{
	int DetaX1;
	int DetaY1;
		
	BulletObject* pObject1 = dynamic_cast <BulletObject*> (m_pMainEngine->GetDisplayableObject(1));
	if(pObject1 == NULL)
	{
		return;
	}
	else
	{
		DetaX1 = abs (this->GetXCentre() - pObject1->GetXCentre());
		DetaY1 = abs (this->GetYCentre() - pObject1->GetYCentre());
		int isfire = pObject1->IsFire();
	
		// if the bullet is fire and the distance between the bullet and the enemy is less than 15,
		// the enemy will be killed
		if((DetaX1 * DetaX1 + DetaY1 * DetaY1 < 15 * 15)&& isfire)
			live = 0;
	}
}


/*	Check the distance between the soldier and enemy object,
	then decide whether the enemy observe the soldier and move to
	kill the soldier
*/
void EnemyObject::IsAction(int iCurrentTime)
{
	SoldierObject*  pObject = dynamic_cast <SoldierObject*>  (m_pMainEngine->GetDisplayableObject(0));
	if(pObject == NULL)
	{
		return;
	}
	else
	{

		int detax = abs(this->GetXCentre() - pObject->GetXCentre());
		int detay = abs(this->GetYCentre() - pObject->GetYCentre());

		

		// if the distance between the player and the enemy is less than 300,
		// the player will be observe and then the enemy will go to kill the player
		 if(!action)
		{
		
			if(detax * detax + detay * detay < 300 * 300)
			{
				action = 1;
				if(live == 1){
					actionTime = iCurrentTime;
				}
			}
			else
			{
				action = 0;
			}
		}
	}
	
}


int EnemyObject::ActionMove(int iCurrentTime)
{
	
	if(iCurrentTime % 6000 < 3000)
	{
		MoveChoice = 0;
	}
	else{
		MoveChoice = 1;
	}
	int move = 1;
	MyProjectTileManager& tm = m_pMainEngine -> GetTileManager();
	SoldierObject*  pObject = dynamic_cast <SoldierObject*>  (m_pMainEngine->GetDisplayableObject(0));
	if(pObject == NULL)
	{
		return -1;
	}
	else
	{
		// get the distances between the player and the enemy
		int mapX = iMapX - (pObject -> getMapX());
		int mapY = iMapY - (pObject -> getMapY());
		int XDirection = 0;
		int YDirection = 0;

		int temp = abs(mapX) - abs(mapY);

		/*	Plan A to move towards the soldier
			Compare the difference between with X coordinate and with Y coordinate
			change the bigger one
		*/
		if(!MoveChoice)
		{
			
			if (mapX >= 0 && temp >=0)
			{
				XDirection = -1;
				direction = 2;
			}
			if (mapX < 0 && temp >= 0)
			{
				XDirection = 1;
				direction = 3;
			}
			if (mapY < 0 && temp < 0)
			{
				YDirection = 1;
				direction = 1;
			}
			if (mapY >= 0 && temp < 0)
			{
				YDirection = -1;
				direction = 0;
			}
			
		}

		/*	Plan B to move towards the soldier
			Compare the difference between with X coordinate and with Y coordinate
			change the smaller one
		*/
		if(MoveChoice)
		{
			if(mapY < 0 && temp >= 0)
			{
				YDirection = 1;
				direction = 1;
			}
			if(mapY > 0 && temp >= 0)
			{
				YDirection = -1;
				direction = 0;
			}

			// if the difference of Y coordinate between soldier and enemy equals to 0, then change X
			if(mapY == 0 && temp >= 0)
			{
				if(mapX <0 )
				{
					XDirection = 1;
					direction = 3;
				}
				if(mapX >= 0 )
				{
					XDirection = -1;
					direction = 2;
				}
			}
			if(mapX <0 && temp < 0)
			{
				XDirection = 1;
				direction = 3;
			}
			if(mapX > 0 && temp < 0)
			{
				XDirection = -1;
				direction = 2;
			}
		
			// if the difference of X coordinate between soldier and enemy equals to 0, then change Y
			if(mapX == 0 && temp < 0 && mapY <= 0)
			{
				YDirection = 1;
				direction = 1;
			}
			if(mapX == 0 && temp < 0 && mapY > 0)
			{
				YDirection = -1;
				direction = 0;
			}
		}

		// check the whether the movement set for next step is valid
		switch ( tm.GetValue( 
				iMapX + XDirection,
				iMapY + YDirection ) )
		{

		case 0: // road
		case 5:	// buff
		case 6: // destination
		case 7: // passageway
		case 8: // passageway
		case 9: // passageway
			// Allow move - set up new movement now
			iMapX += XDirection;
			iMapY += YDirection;	
			break;

		case 1: // building
		case 2: // tree
		case 3: // river
		case 4: // fort
			// the next tail is not allowed to move

			// if XDirection == 0, therefore the movement set previously is in Y direction
			// when can change X direction since the Y direction movement is invalid
			if(!XDirection)
			{
				int invalid = 1;
				int temp = 0;

				// get the correct direction in case of X coordinate
				if(mapX > 0)
				{
					XDirection = -1;
					direction = 2;
				}
				if(mapX <= 0)
				{
					XDirection = 1;
					direction = 3;
				}

				// loop to find a solution to get around of the invalid tile
				while (invalid)
				{
					// check if valid to get around by the next tile
					switch (tm.GetValue( 
					iMapX + XDirection,
					iMapY + YDirection ))
					{
					case 0: // road
					case 5:	// buff
					case 6: // destination
					case 7: // passageway
					case 8: // passageway
					case 9: // passageway
						// Allow move - set up new movement now
						iMapX += XDirection;
						// sign value to move their set the correct time to move
						move = temp;
						invalid = 0;
						// change the move plan
						if(MoveChoice){MoveChoice = 0;}
						if(!MoveChoice){MoveChoice = 1;}
					break;
					case 1: // building
					case 2: // tree
					case 3: // river
					case 4: // fort
						if(XDirection > 0){XDirection++;}
						if(XDirection < 0){XDirection--;}
						break;

					}
					
					// if still no solution after 15 times move, jump out the loop
					if(temp >= 15)
					{
						invalid = 0;
					}
					temp++;
				}
			}

			
			// if YDirection == 0, therefore the movement set previously is in X direction
			// when can change Ydirection since the X direction movement is invalid
			if(!YDirection)
			{
				int invalid = 1;
				int temp = 0;

				// get the direction
				if(mapY > 0)
				{
					YDirection = -1;
					direction = 0;
				}
				if(mapY <= 0)
				{
					YDirection = 1;
					direction = 1;
				}


				while (invalid)
				{
					switch (tm.GetValue( 
						iMapX + XDirection,
						iMapY + YDirection ))
					{
					case 0: // road
					case 5:	// buff
					case 6: // destination
					case 7: // passageway
					case 8: // passageway
					case 9: // passageway
						// Allow move - set up new movement now
						iMapY += YDirection;
						// sign value to move their set the correct time to move
						move = temp;
						invalid = 0;
						// change the move plan
						if(MoveChoice){MoveChoice = 0;}
						if(!MoveChoice){MoveChoice = 1;}
					break;
					case 1: // building
					case 2: // tree
					case 3: // river
					case 4: // fort
						if(YDirection > 0){YDirection++;}
						if(YDirection < 0){YDirection--;}
						break;

					}
					
					// if still no solution after 15 times move, jump out the loop
					if(temp >= 15)
					{
						invalid = 0;
						move = 1;
					}
					temp++;
				}
			}
			break;
		}

	}
	return move;	
}

/*	In case of the enemy has not observe the soldier,
	set a random move for the enemy
*/
void EnemyObject::NonActionMove(void)
{
	MyProjectTileManager& tm = m_pMainEngine -> GetTileManager();
	int isvalid = 1;
	int XDirection = 0;
	int YDirection = 0;
	while(isvalid)
	{
		// randomly generate the next tile that enemy move to
		// up, down, left or right
		switch(rand()%4)
		{
		case 0: // left
			XDirection = -1;
			YDirection = 0;
			direction = 2;
			break;
		case 1: // right
			XDirection = 1;
			YDirection = 0;
			direction = 3;
			break;
		case 2: // up
			XDirection = 0;
			YDirection = -1;
			direction = 0;
			break;
		case 3: // down
			XDirection = 0;
			YDirection = 1;
			direction = 1;
			break;
		}

		// check whether the movement is valid
		switch ( tm.GetValue( 
				iMapX + XDirection,
				iMapY + YDirection ) )
		{
		
		case 0: // road
		case 5:	// buff
		case 6: // destination
		case 7: // passageway
		case 8: // passageway
		case 9: // passageway
			// the movement is valid therefore jump out of the loop
			isvalid = 0;
			break;
		}
	
	}

	// set the movement
	iMapX += XDirection;
	iMapY += YDirection;
	
}



