#include "MyProjectTileManager.h"
#include "JPGImage.h"

MyProjectTileManager::MyProjectTileManager(void)
{
}


MyProjectTileManager::~MyProjectTileManager(void)
{
}


int MyProjectTileManager::GetTileWidth(void)
{
	return 20;
}


int MyProjectTileManager::GetTileHeight(void)
{
	return 20;
}


void MyProjectTileManager::DrawTileAt(
	BaseEngine* pEngine, 
	SDL_Surface* pSurface, 
	int iMapX, int iMapY, 
	int iStartPositionScreenX, int iStartPositionScreenY)
{
	ImageData im, im2;
	switch( GetValue(iMapX,iMapY) )
	{
	case 0: 
	case 1:
	case 3:
	case 6:
		break;
	case 2:
		im2.LoadImage("tree.png");
		im.ResizeTo(&im2, 20, 20, true);
		im.RenderImageWithMask(pEngine->GetBackground(),
			0, 0,
			iStartPositionScreenX,iStartPositionScreenY,
			im.GetWidth(), im.GetHeight());
		break;
	
	case 4:
		im2.LoadImage("fort.png");
		im.ResizeTo(&im2, 20, 20, true);
		im.RenderImageWithMask(pEngine->GetBackground(),
			0, 0,
			iStartPositionScreenX,iStartPositionScreenY,
			im.GetWidth(), im.GetHeight());
		break;
	case 5:
		im2.LoadImage("buff.png");
		im.ResizeTo(&im2, 20, 20, true);
		im.RenderImageWithMask(pEngine->GetBackground(),
			0, 0,
			iStartPositionScreenX,iStartPositionScreenY,
			im.GetWidth(), im.GetHeight());
		break;
	case 7:
	case 8:
	case 9:
		im2.LoadImage("backtrans.png");
		im.ResizeTo(&im2, 20, 20, true);
		im.RenderImageWithMask(pEngine->GetBackground(),
			0, 0,
			iStartPositionScreenX,iStartPositionScreenY,
			im.GetWidth(), im.GetHeight());
		break;
	}
}
