#pragma once
#include "displayableobject.h"
#include "MovementPosition.h"
#include "JPGImage.h"


class MyProjectMain;

class EnemyObject :
	public DisplayableObject
{
public:
	EnemyObject(MyProjectMain* eEngine);
	~EnemyObject(void);
	void Draw(void);
	void DoUpdate(int iCurrentTime);
	int Islive(){return live;}
	int getMapX(){return iMapX;}
	int getMapY(){return iMapY;}
	int getAction(){return action;}

private:
	MyProjectMain* m_pMainEngine;
	MovementPosition m_oMover;
	int iMapX;
	int iMapY;
	int live;
	ImageData im1, im2, im3, im4, im5, im6, im7, im8, im, im0;
	int action;
	int direction;
	int imgIndex;
	void initialPosition(void);
	void CheckLive(void);
	void IsAction(int iCurrentTime);
	int ActionMove(int iCurrentTime);
	void NonActionMove(void);
	int actionTime;

public:
	int MoveChoice;
};

