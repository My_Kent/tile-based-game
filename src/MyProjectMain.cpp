#include "header.h"
#include "BaseEngine.h"
#include "MyProjectMain.h"
#include "JPGImage.h"
#include "TileManager.h"
#include "DisplayableObject.h"
#include "SoldierObject.h"
#include "EnemyObject.h"
#include "BulletObject.h"
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;
 

void MyProjectMain::SetupBackgroundBuffer()
{
	ImageData im, im2, im3;
	switch (m_State)
	{
	case stateInit:
		// load the image for the initial state and resize it to correct size
		im2.LoadImage( "initial.jpg" );
		im.ResizeTo(&im2, 800, 600, true);
		im.RenderImage( this->GetBackground(), 
				0, 0, 
				0, 0, 
				im.GetWidth(), im.GetHeight() );
		break;

	case stateTutorial: 
		// load the image for the tutorial state and resize it to correct size
		im2.LoadImage( "tutorial.jpg" );
		im.ResizeTo(&im2, 800, 600, true);
		im.RenderImage( this->GetBackground(), 
				0, 0, 
				0, 0, 
				im.GetWidth(), im.GetHeight() );
		break;

	case stateStart:
		
		im.LoadImage("start.jpg");
		im2.ResizeTo(&im, 800, 600, true);
		im.LoadImage( "startButton.jpg" );
		im3.ResizeTo(&im, 100, 40, true);

		
		im2.RenderImage( this->GetBackground(), 
				0, 0, 
				0, 0, 
				im2.GetWidth(), im2.GetHeight() );

		im3.RenderImage( this->GetBackground(), 
				0, 0, 
				680, 550, 
				im3.GetWidth(), im3.GetHeight() );
		/* Update values for some tiles.
		**	More specific, tiles such as tree, buff and fort maybe
		**	changed by the player, the following codes are make
		**	them valid for the next level of the game
		*/
		for(int i = 0; i < 38; i++)
		{
			for(int j = 0; j < 28; j++)
			{
				switch (m_oTiles.GetValue(j, i))
				{
				case 7:
					m_oTiles.SetValue(j, i, 2);
					break;
				case 8:
					m_oTiles.SetValue(j, i, 4);
					break;
				case 9:
					m_oTiles.SetValue(j, i, 5);
					break;

				}
			}
		}
		
		break;
	case stateHighScore:
		// load the image for the highscore state and resize it to correct size
		im2.LoadImage( "highscore.jpg" );
		im.ResizeTo(&im2, 800, 600, true);
		im.RenderImage( this->GetBackground(), 
				0, 0, 
				0, 0, 
				im.GetWidth(), im.GetHeight() );
		break;

	case stateMain:
		FillBackground(0x000000);
		// load the image head and background for the main section
		im.LoadImage("head.png");
		im2.ResizeTo(&im, 720, 80, true);
		im.LoadImage( "background.jpg" );
		im3.ResizeTo(&im, 720, 520, true);

		im2.RenderImage( this->GetBackground(), 
				0, 0, 
				45, 0, 
				im2.GetWidth(), im2.GetHeight() );

		im3.RenderImage( this->GetBackground(), 
				0, 0, 
				45, 80, 
				im3.GetWidth(), im3.GetHeight() );

		// draw the tile according to the value got for the tile
		m_oTiles.DrawAllTiles( this, 
			this->GetBackground(), 
			0, 0, 37, 27 );

		// draw the specific number of rectangle on the background
		// to stand for the number of life that the player left
		for(int i = 0; i < lifeNumber; i++)
		{
			DrawBackgroundRectangle(90+i*22,58,108+i*22,65,0x77e205);
		}
		
		break;

	case statePaused:  break;
	case stateDie:  break;
	case stateEnd:
		// load the image head and background for the main section
		im2.LoadImage( "gameover.jpg" );
		im.ResizeTo(&im2, 800, 600, true);
		im.RenderImage( this->GetBackground(), 
				0, 0, 
				0, 0, 
				im.GetWidth(), im.GetHeight() );
		break;
	}

}



int MyProjectMain::InitialiseObjects()
{
	
	DrawableObjectsChanged();
	// Destroy any existing objects
	DestroyOldObjects();

	// Create an array to store the displayableobject
	m_ppDisplayableObjects = new DisplayableObject*[2*level + 4];

	// the first element is soldierobject which is controled by the player
	// the second element is bulletobject, it will appear when the player choose to fire
	m_ppDisplayableObjects[0] = new SoldierObject(this);
	m_ppDisplayableObjects[1] = new BulletObject(this);

	// the rest spaces except the last one are used to store the enemy object
	// the first level of the game has 3 enemy, as the level increasing 1, the number of enemy will increase 2
	for(int i = 2; i < 2* level  + 3; i++)
	{
		m_ppDisplayableObjects[i] = new EnemyObject(this);

	}
	m_ppDisplayableObjects[2*level + 3] = NULL;
	return 0;
}



void MyProjectMain::DrawStrings()
{
	// Build the string to print
	
	switch( m_State )
	{

	case stateStart:
		// print the string to tell the player the level he/she is going to play
		char buf1[128];
		sprintf( buf1, "level %6d",  level);
		CopyBackgroundPixels( 450, 440, GetScreenWidth(), 40);
		DrawScreenString( 450, 440, buf1, 0xffff, NULL );
		SetNextUpdateRect( 450, 440, GetScreenWidth(), 40);
		break;
	

		// in the highscore state, the top 10 highest level that play has reached
	case stateHighScore:
		
		{
		// read the data from the file and store the values in the int[] ibuff
		ifstream file("Highscore.txt");
		int score;
		int ibuff[10];
		int index = 0;
		if(file.is_open())
		{
			while(file.good())
			{
				file >> score;
				ibuff[index] = score;
				index++;
			}
			file.close();
		}
		
		// print the data on the screen on the specific location
		for(int i = 0; i < 10; i++)
		{
			char sbuf[10];
			sprintf( sbuf, "%d",  ibuff[i]);
			// Clearthe screen, since we about to draw text on it.
			CopyBackgroundPixels( 120, i*40 + 120, GetScreenWidth(), 50 );
			// Then draw the strings
			DrawScreenString( 120, i*40 + 120, sbuf, 0x888888, NULL );
			// And mark that area of the screen as having been changed, so it gets redrawn
			SetNextUpdateRect( 120/*X*/, i*40 + 120/*Y*/, GetScreenWidth(), 50/*Height*/ );
		}
		}
		break;

	case stateMain:
		// draw string on the screen how many life left for the game
		char buf[128];
		sprintf( buf, "Lv.%2d",  level);
		// Clear the top of the screen, since we about to draw text on it.
		CopyBackgroundPixels( 120, 20, GetScreenWidth(), 25 );
		// Then draw the strings
		DrawScreenString( 120, 20, buf, 0xffffff, NULL );
		// And mark that area of the screen as having been changed, so it gets redrawn
		SetNextUpdateRect( 120/*X*/, 20/*Y*/, GetScreenWidth(), 25/*Height*/ );
		break;
	case statePaused:
		// draw string to state that the game is paused and if want to continue, press SPACE key
		CopyBackgroundPixels( 0/*X*/, 280/*Y*/, GetScreenWidth(), 40/*Height*/ );
		DrawScreenString( 200, 300, "Paused. Press SPACE to continue", 0xffffff, NULL );
		SetNextUpdateRect( 0/*X*/, 280/*Y*/, GetScreenWidth(), 40/*Height*/ );
		break;
	case stateDie:
		// draw string to notify the player the soldier is killed
		char buf2[128];
		sprintf( buf2, "Unfortunate die, press SPACE to have other chance",  lifeNumber);
		// Clear the top of the screen, since we about to draw text on it.
		CopyBackgroundPixels( 20/*X*/, 280/*Y*/, GetScreenWidth(), 40/*Height*/ );
		// Then draw the strings
		DrawScreenString( 20, 280, buf2, 0xffff, NULL );
		SetNextUpdateRect( 20/*X*/, 280/*Y*/, GetScreenWidth(), 40/*Height*/ );
		break;
	
	}



}


/* Overridden GameAction which can be modified */
void MyProjectMain::GameAction()
{
	// If too early to act then do nothing
	if ( !TimeToAct() )
		return;

	// Don't act for another 20 ticks
	SetTimeToAct( 20 );
	switch( m_State )
	{
	case stateInit:
	case statePaused:
	case stateTutorial:
	case stateStart:
		break;
	case stateEnd:
		// load the data from highscore file and compare with 
		// the current level that player has reached. If the player played top 10 result
		// store it in the file
		{
		ifstream file("Highscore.txt");
		int score;
		int buff[10];
		int index = 0;
		if(file.is_open())
		{
			while(file.good())
			{
				file >> score;
				buff[index] = score;
				index++;
			}
		file.close();
		}

		ofstream ofile;
		ofile.open("Highscore.txt");
		int temp = 0;

		// compare the result of this game and the top 10 history result
		for(int i = 0; i < 10; i++)
		{
			if(level >= buff[temp])
			{
				ofile << level << "\n";
				level = 1;
			}
			else{
				ofile << buff[temp] << "\n";
				temp++;
			}
			
		}
		ofile.close();
		}
		break;
	case stateDie:
		break;
	case stateMain:
		// Only tell objects to move when not paused etc
		UpdateAllObjects( GetTime() );
		break;
	}

}


// Override to handle a mouse press
void MyProjectMain::MouseDown( int iButton, int iX, int iY )
{
	switch(m_State)
	{
	case stateInit:
		// the mouse is clicked on the location which has "New Game" on the screen
		// change the state to stateStart
		if( iX >=115 && iX <=285 && iY >=287 && iY <= 325)
		{
			m_State = stateStart;
			lifeNumber = 5;
			level = 1;
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}

		// the mouse is clicked on the location which has "Tutorial" on the screen
		// change the state to stateTutorial
		if(iX >=115 && iX <=285 && iY >=343 && iY <= 380)
		{
			m_State = stateTutorial;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}

		// the mouse is clicked on the location which has "HighScore" on the screen
		// change the state to stateHighScore
		if(iX >=115 && iX <=285 && iY >=400 && iY <= 440)
		{
			m_State = stateHighScore;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}

		// the mouse is clicked on the location which has "Exit" on the screen
		// Exit the game
		if(iX >=115 && iX <=285 && iY >=457 && iY <= 497)
		{
			SetExitWithCode( 0 );
		}
		break;

	case stateTutorial:
		// the mouse is clicked on the location which has "Back" on the screen
		// change the state back to stateInit
		if(iX >=28 && iX <=178 && iY >=524 && iY <= 564)
		{
			m_State = stateInit;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}
		break;
	case stateHighScore:
		// the mouse is clicked on the location which has "Back" on the screen
		// change the state back to stateInit
		if(iX >=10 && iX <=160 && iY >=540 && iY <= 580)
		{
			m_State = stateInit;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}
		break;
	case stateEnd:
		// the mouse is clicked on the location which has "New game" on the screen
		// change the state back to stateInit
		if(iX >=20 && iX <=198 && iY >=443 && iY <= 485)
		{
			m_State = stateInit;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}
		// the mouse is clicked on the location which has "Exit" on the screen
		// Exit the game
		if(iX >=20 && iX <=198 && iY >=512 && iY <= 552)
		{
			SetExitWithCode( 0 );
		}

		break;
	case stateStart:
		if(iY >= 550 && iY <= 590 && iX >= 680 && iX <= 780)
		{
			m_State = stateMain;
			InitialiseObjects();
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		}
	}
	// Redraw the background
	
}

/*
Handle any key presses here.
Note that the objects themselves (e.g. player) may also check whether a key is currently pressed
*/
void MyProjectMain::KeyDown(int iKeyCode)
{
	switch ( iKeyCode )
	{
	case SDLK_ESCAPE: // End program when escape is pressed
		SetExitWithCode( 0 );
		break;
	case SDLK_SPACE: // SPACE Pauses
		switch( m_State )
		{
		// change to state to statemain
		case stateStart:
			m_State = stateMain;
			InitialiseObjects();
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		case stateMain:
			// Go to state paused
			m_State = statePaused;
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		case statePaused:
			// Go to state main
			m_State = stateMain;
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		case stateDie:
			// Go to state main			
			m_State = stateMain;
			// some problem with InitialiseObject
			InitialiseObjects();
			// Force redraw of background
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		} // End switch on current state
		break; // End of case SPACE
	}
}


void MyProjectMain::DrawChanges()
{
	// NEW IF
	if ( m_State == stateInit )
		return; // Do not draw objects if initialising

	// Remove objects from their old positions
	UndrawChangingObjects();
	// Draw the text for the user
	DrawStrings();
	// Draw objects at their new positions
	DrawChangingObjects();
}

void MyProjectMain::DrawScreen()
{
	// First draw the background
	//this->CopyBackgroundPixels( 100, 100, 100, 100 );
	CopyAllBackgroundBuffer();
	// And finally, draw the text
	DrawStrings();

	// NEW IF
	switch(m_State)
	{
	case stateInit:
	case stateTutorial:
	case stateStart:
	case stateDie:
	case stateEnd:
		return;

	case stateMain:
	case statePaused:
		// Then draw the changing objects
		DrawChangingObjects();
		break;
	}
}