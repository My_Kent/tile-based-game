#include "DisplayableObject.h"
#include "SoldierObject.h"
#include "MyProjectMain.h"
#include "MovementPosition.h"
#include "EnemyObject.h"

#define up 1;
#define down 2;
#define left 3;
#define right 4;

SoldierObject::SoldierObject(MyProjectMain* pEngine)
	: DisplayableObject(pEngine)
	, m_pMainEngine(pEngine)
	, m_iMapX(1)
	, m_iMapY(1)
	, direction(2)
	, imgIndex(0)
	, speed(300)
	, buffTime(0)
	, hasBuff(0)
{
	m_iStartDrawPosX = 0;
	m_iStartDrawPosY = 0;
	// Current and previous coordainates for the object
	m_iPreviousScreenX = m_iCurrentScreenX = m_iMapX *20 + 25;
	m_iPreviousScreenX = m_iCurrentScreenY = m_iMapY *20 + 60;
	m_iDrawWidth = 20;
	m_iDrawHeight = 20;
	SetVisible(true);

	im.LoadImage( "eup1.png" );
	im1.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "eup2.png" );
	im2.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "edown1.png" );
	im3.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "edown2.png" );
	im4.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	
	im.LoadImage( "eleft1.png" );
	im5.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "eleft2.png" );
	im6.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "eright1.png" );
	im7.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	im.LoadImage( "eright2.png" );
	im8.ResizeTo(&im, m_iDrawWidth, m_iDrawHeight, false);
	

}


SoldierObject::~SoldierObject(void)
{
}


void SoldierObject::Draw(void)
{

	switch(direction)
	{
	case 1:// up
		if(imgIndex)
			im1.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im1.GetWidth(), im1.GetHeight());
		else
			im2.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im2.GetWidth(), im2.GetHeight());
		break;
	case 2:// down
		if(imgIndex)
			im3.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im3.GetWidth(), im3.GetHeight());
		else
			im4.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im4.GetWidth(), im4.GetHeight());
		break;
	case 3:// left
		if(imgIndex)
			im5.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im5.GetWidth(), im5.GetHeight());
		else
			im6.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im6.GetWidth(), im6.GetHeight());
		
		break;
	case 4:// right
		if(imgIndex)
			im7.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im7.GetWidth(), im7.GetHeight());
		else
			im8.RenderImageWithMask( m_pEngine->GetForeground(), 
					0, 0, 
					m_iCurrentScreenX,m_iCurrentScreenY, 
					im8.GetWidth(), im8.GetHeight());
		break;
	
	}
	
	StoreLastScreenPositionAndUpdateRect();
}


void SoldierObject::DoUpdate(int iCurrentTime)
{
	// set time to switch between images
	if(iCurrentTime % 400 < 200)
	{
		imgIndex = 1;
	}
	else{
		imgIndex = 0;
	}
	
	m_iPreviousScreenX = m_iCurrentScreenX;
	m_iPreviousScreenY = m_iCurrentScreenY;
	
	
	// if the soldier got a buff, set a timer to calculate
	// the buff will release after 5 seconds
	if(hasBuff)
	{
		if(iCurrentTime >= buffTime + 5000)
		{
			speed = 100;
			hasBuff = 0;
		}
	}
	
	// check whether the soldier has died
	int die = IsDied();

	// in the case that soldier died
	if(die == 1 )
	{
		// when there is life left
		if(m_pMainEngine->getLife() >1)
		{
			// decrease the life number by 1 and change the state to StateDie
			m_pMainEngine->changeLife(-1);
			m_pMainEngine->changeState(2);
		}
		// when there is no more life left for soldier to play
		else
		{
			// change state to game over
			m_pMainEngine->changeState(3);
		}
	}
		
	// in the case that soldier not die
	else
	{	
		MyProjectTileManager& tm = m_pMainEngine -> GetTileManager();
		
		// check the current position 
		switch ( tm.GetValue( m_iMapX, m_iMapY))
		{
		// if on the buff tile, update the tile and get the buff
		case 5: tm.UpdateTile( m_pMainEngine, m_iMapX, m_iMapY,
				9);
				
			{
			int temp = iCurrentTime % 2;
			buffTime = iCurrentTime;
			hasBuff = 1;
			speed = 500;
				
			if(temp)
				speed = 100;
			else
				speed = 500;	
			}
			break;
		// if on the destination tile
		case 6: 
			{
				// check whether all the enemy is killed
				int complete = 1;
				int level = m_pMainEngine->getLevel();
				for(int i = 2; i < 2 * level + 3; i++)
				{

					EnemyObject* pObject = dynamic_cast <EnemyObject*> (m_pMainEngine->GetDisplayableObject(i));
					if(pObject == NULL)
						return;
					else
					{
						int valid = pObject -> Islive();
						//TODO, enter the next level of the game;
						if(valid)
						{
							complete = 0;
							break;
						}
					}
				}
				// if the mission is completed, move to the next level of the game
				if(complete)
				{
					m_pMainEngine -> changeState(1);
					m_pMainEngine -> changeLevel();
				}
			}
			break; 
		}


		if( m_oMover.HasMovementFinished(iCurrentTime) )
		{
			

			int X_Direction = 0;
			int Y_Direction = 0;
			// set movement according to the direction key that player pressed
			// only allow single direction movement, either X or Y
			if ( m_pMainEngine->IsKeyPressed( SDLK_UP ) )
			{
				Y_Direction = -1;
				direction  = up;
			}
			if ( !(m_pMainEngine->IsKeyPressed( SDLK_UP ) ||m_pMainEngine->IsKeyPressed( SDLK_DOWN )) && m_pMainEngine->IsKeyPressed( SDLK_RIGHT ) )
			{	
				X_Direction = 1;
				direction = right;
			}
			if ( m_pMainEngine->IsKeyPressed( SDLK_DOWN ) )
			{
				Y_Direction = 1;
				direction = down;
			}
			if ( !(m_pMainEngine->IsKeyPressed( SDLK_UP ) ||m_pMainEngine->IsKeyPressed( SDLK_DOWN )) && m_pMainEngine->IsKeyPressed( SDLK_LEFT ) )
			{
				X_Direction = -1;
				direction = left;
			}
				

			switch ( tm.GetValue( 
					m_iMapX + X_Direction,
					m_iMapY + Y_Direction ) )
			{
			case 0: // road
			case 5:	// buff
			case 6: // destination
			case 7: // passageway
			case 8: // passageway
			case 9: // passageway
				// Allow move - set up new movement now
				m_iMapX += X_Direction;
				m_iMapY += Y_Direction;
				m_oMover.Setup(
					m_iCurrentScreenX,
					m_iCurrentScreenY,
					m_iMapX * 20 + 25,
					m_iMapY * 20 + 60,
					iCurrentTime,
					iCurrentTime + speed);
				break;
			}
		}
		
		if ( !m_oMover.HasMovementFinished(iCurrentTime) )
		{
			
				// Ask the mover where the object should be
				m_oMover.Calculate( iCurrentTime );
				m_iCurrentScreenX = m_oMover.GetX();
				m_iCurrentScreenY = m_oMover.GetY();
		}
	}
	
	
	RedrawObjects();
}


int SoldierObject::IsDied(void)
{
	int die = 0;
	int level = m_pMainEngine->getLevel();
	for(int i = 2; i < 2 * level + 3; i++)
	{
		EnemyObject* pObject = dynamic_cast <EnemyObject*> (m_pMainEngine->GetDisplayableObject(i));
		if(pObject == NULL)
		{
			return -1;
		}
		else
		{
			// check the distance between the solider and the enemy
			int DitaX1 = abs (this->GetXCentre() - pObject->GetXCentre());
			int DitaY1 = abs (this->GetYCentre() - pObject->GetYCentre());
			int validEnemy  = pObject -> Islive();
			// if the distance is smaller than a specific one and the enemy is stll alive, then die
			if ((DitaX1 * DitaX1 + DitaY1 * DitaY1 < 15 * 15) && validEnemy)
			{
				die = 1;
				return die;
			}

			// check the distance between the solider and the enemy
			// if the enemy choose to self-damage to kill the soldier,
			// the solider will die if the distance is smaller than a specific one
			int temp1 = abs(m_iMapX - pObject -> getMapX());
			int temp2 = abs(m_iMapY - pObject -> getMapY());
			int bombed = pObject ->getAction();
			if( bombed == 2 && (temp1 * temp1 + temp2 * temp2 <= 8) && validEnemy)
			{
				die = 1;
				return die;
			}
		}
	}
	return die;
}
