#ifndef MYPROJECTMAIN_H
#define MYPROJECTMAIN_H

#include "BaseEngine.h"
#include "MyProjectTileManager.h"

class MyProjectMain : public BaseEngine
{
public:

	/**
	Constructor
	*/
	MyProjectMain()
	: BaseEngine( 6 )
	, lifeNumber(5)
	, m_State(stateInit)
	, level(1)
	{
			{
			char* data[] = {

				"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
				"baaaaaaaacaaaaaaaaaacaaaaafaaaaaaaaaab",
				"baaaaaaaacaaaaaaaaaacaaaaaaaaaaaaaaaab",
				"bbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
				"bbbbbaaaabbbbbbbbbbbbaaaaaabbbaaaaabbb",
				"bbbbbaaaabbbbbbbbbbbbaaaaaabbbaaaaabbb",
				"bbbbbaaaaaaaaaaaaaaaaaaaaaabbbaaaaabbb",
				"bbbbbaaaaaaaaaaaaaaaaeeaaaaaaaaaaaabbb",
				"bbbbbaaaaaaaaaaaaaaaaeeaaaaaaaaaaaabbb",
				"bbbbbaaaafaaaaaaaaaaaeeaaaaaaaaaaaabbb",
				"bbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbb",
				"bbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
				"bbbbbaaaabbbbbcabbbbbaaaaaaaaaaaaaaaab",
				"bbbbbaaaabbbbbcabbbbbaaaaaabbbaaaaaaab",
				"bbbbbaaaabbbbbcabbbbbaafaaabbbaaaaaaab",
				"bbbbbeaeabbbbbcabbbbbaaaaaabbbaaaaaaab",
				"bbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
				"baaaaaaaaaaaaaaaaaaaaabbbbbbbbaaaaaaab",
				"bddddddddddddddddddaaabbbbbbbbaaaaaaab",
				"bddddddddddddddddddaaaaaaaaaaaaaaaaaab",
				"bddddddddddddddddddaaaaaaaaaaaaaaaaaab",
				"bddddddddddddddddddaaaaaaaaaaaaaaaaaab",
				"baaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbaaaab",
				"baaaaaaaaaaaaaeeaaaaaaaaaaaaaaabbaaaab",
				"bagggaaaaaaaaaeeaaaaaaaaaaaaaaabbaaaab",
				"bagggaaaaaaaaaeeaaaaaaaaaaaaaaabbaaaab",
				"baaaaaaaaaaaaaeeaaaaaaaaaaaaaaabbaaaab",
				"bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"				

				};

			// Specify how many tiles wide and high
			m_oTiles.SetSize( 38, 28 ); 
			// Set up the tiles
			for ( int x = 0 ; x < 38; x++ )
				for ( int y = 0 ; y < 28 ; y++ )
					m_oTiles.SetValue( x, y, data[y][x]-'a' );

			for ( int y = 0 ; y < 28 ; y++ )
			{
				for ( int x = 0 ; x < 38 ; x++ )
					printf("%d ", m_oTiles.GetValue(x,y) );
				printf("\n" );
			}

			// Specify the screen x,y of top left corner
			m_oTiles.SetBaseTilesPositionOnScreen( 25, 60 );
		}
	}

	// Do any setup of back buffer prior to locking the screen buffer
	// Basically do the drawing of the background in here and it'll be copied to the screen for you as needed
	virtual void SetupBackgroundBuffer();

	// Create any moving objects
	int InitialiseObjects();

	/** Draw any strings */
	void DrawStrings();

	/**
	The game logic - move things and change the state if necessary.
	Must call Redraw(true/false) if anything changes that shows on the screen.
	*/
	virtual void GameAction();

	// Handle pressing of a mouse button
	void MouseDown( int iButton, int iX, int iY );

	// Handle pressing of a key
	virtual void KeyDown(int iKeyCode);

public:

	void changeLife(int change){lifeNumber = lifeNumber + change;}
	int getLife(void){return lifeNumber;}
	enum State { stateInit, stateTutorial, stateHighScore, stateStart,stateMain, statePaused, stateDie, stateEnd };
	void DrawChanges();
	void DrawScreen();
	MyProjectTileManager& GetTileManager() { return m_oTiles; }
	int getLevel(){return level;}
	void changeState(int i)
	{	
		switch(i)
		{
		case 1: m_State = stateStart;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		case 2: m_State = stateDie;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
			break;
		case 3: m_State = stateEnd;
			SetupBackgroundBuffer();
			// Redraw the whole screen now
			Redraw(true);
		}
	}
	void changeLevel(void){ level++;}

private:
	int lifeNumber;
	State m_State;
	MyProjectTileManager m_oTiles;
	int level;
	
};

#endif
